# poetry add lxml beautifulsoup4 click

from pathlib import Path
from typing import List
from lxml import etree
import click

DEPRECATED_REPLACEMENTTEXT_CHILDREN = {
    'playerSelf',
    'playerNPC',
    'playerNPCRough',
    'NPCSelf',
    'NPCPlayer',
    'NPCPlayerRough',
    'NPCOtherNPC',
    'NPCOtherNPCRough',
}
def clothing_check_replacementText(t: etree._ElementTree, msgs: List[str]) -> None:
    #e: etree._Element
    for e in t.xpath('//replacementText|//displacementText'):
        #print(repr(e))
        for deprecated_tag in DEPRECATED_REPLACEMENTTEXT_CHILDREN:
            if (found := e.find(deprecated_tag)) is not None:
                msgs.append(click.style(f'W: {t.getpath(found)} is deprecated. Use <self>, <other>, and <otherRough> instead.', fg='yellow'))

def handle_clothing_xml(filename: Path) -> None:
    msgs = []
    try:
        t = etree.parse(str(filename.absolute()))
    except:
        msgs += [click.style(f'E: Failed to parse {filename}!', fg='red')]
    if len(msgs) == 0:
        clothing_check_replacementText(t,msgs)
    if len(msgs):
        click.echo(f'{filename}:')
        for msg in msgs:
            click.echo(f'  {msg}')
        

def main():
    for resSubDir in Path('res').iterdir():
        if resSubDir.is_dir():
            if resSubDir.name == 'clothing':
                for clothingFile in resSubDir.rglob('*.xml'):
                    handle_clothing_xml(clothingFile)

if __name__ == "__main__":
    main()
